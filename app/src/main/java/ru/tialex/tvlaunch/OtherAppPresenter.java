package ru.tialex.tvlaunch;

import android.graphics.drawable.Drawable;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.Presenter;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import static android.view.Gravity.CENTER_HORIZONTAL;

public class OtherAppPresenter extends Presenter {
    private static final String TAG = "TVAppPresenter";

    private static final int CARD_WIDTH = 100;
    private static final int CARD_HEIGHT = 100;
    private static int sSelectedBackgroundColor;
    private static int sDefaultBackgroundColor;
    private static Drawable mDefaultImage;

    private static void updateCardBackgroundColor(LinearLayout view, boolean selected) {
        int color = selected ? sSelectedBackgroundColor : sDefaultBackgroundColor;
        view.setBackgroundColor(color);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        Log.d(TAG, "onCreateViewHolder");

        sDefaultBackgroundColor = parent.getResources().getColor(R.color.default_background);
        sSelectedBackgroundColor = parent.getResources().getColor(R.color.selected_background);
        mDefaultImage = parent.getResources().getDrawable(R.drawable.def_icon);

        final TextView textView = new TextView(parent.getContext());
        textView.setPadding(5,5,5,5);
        textView.setGravity(CENTER_HORIZONTAL);
        textView.setSingleLine(true);
        textView.setTextAppearance(parent.getContext(), android.R.style.TextAppearance_Small);
        textView.setEllipsize(TextUtils.TruncateAt.MARQUEE);

        textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        ImageView imageView = new android.support.v7.widget.AppCompatImageView(parent.getContext());

        LinearLayout linearLayout = new LinearLayout(parent.getContext()) {
            @Override
            public void setSelected(boolean selected) {
                updateCardBackgroundColor(this, selected);
                super.setSelected(selected);
            }
        };
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.addView(imageView);
        linearLayout.addView(textView);

        linearLayout.setFocusable(true);
        linearLayout.setFocusableInTouchMode(true);
        updateCardBackgroundColor(linearLayout, false);
        return new ViewHolder(linearLayout);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        ApplicationInfo applicationInfo = (ApplicationInfo) item;
        LinearLayout linearLayout = (LinearLayout) viewHolder.view;
        ImageView imageView = (ImageView) linearLayout.getChildAt(0);
        TextView textView = (TextView) linearLayout.getChildAt(1);

        Log.d(TAG, "onBindViewHolder");
        if (applicationInfo.getActivityClass() != null) {
            Drawable tempDrawable = applicationInfo.getIconDrawable();
            linearLayout.setLayoutParams(new FrameLayout.LayoutParams(CARD_WIDTH, FrameLayout.LayoutParams.WRAP_CONTENT));
            imageView.setLayoutParams(new LinearLayout.LayoutParams(CARD_WIDTH, CARD_HEIGHT));
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            imageView.setImageDrawable((tempDrawable != null) ? applicationInfo.getIconDrawable() : mDefaultImage);
            textView.setText(applicationInfo.getActivityName());
        }
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        Log.d(TAG, "onUnbindViewHolder");
        LinearLayout linearLayout = (LinearLayout) viewHolder.view;
        ImageView imageView = (ImageView) linearLayout.getChildAt(0);
        imageView.setImageDrawable(null);
    }
}
