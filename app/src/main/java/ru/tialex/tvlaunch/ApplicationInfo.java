package ru.tialex.tvlaunch;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

public class ApplicationInfo implements Serializable {
    static final long serialVersionUID = 727566175075960653L;
    private String activityName;
    private String packageName;
//    private Drawable bannerDrawable;
    private Drawable iconDrawable;
    private String activityClass;

    public ApplicationInfo() {
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

//    public Drawable getBannerDrawable() {
//        return bannerDrawable;
//    }
//
//    public void setBannerDrawable(Drawable bannerDrawable) {
//        this.bannerDrawable = bannerDrawable;
//    }

    public Drawable getIconDrawable() {
        return iconDrawable;
    }

    public void setIconDrawable(Drawable iconDrawable) {
        this.iconDrawable = iconDrawable;
    }

    public String getActivityClass() {
        return activityClass;
    }

    public void setActivityClass(String activityClass) {
        this.activityClass = activityClass;
    }

    @Override
    public String toString() {
        return "App: {" +
                "name=" + activityName +
                ", package='" + packageName + '\'' +
                ", class='" + activityClass + '\'' +
                '}';
    }
}
