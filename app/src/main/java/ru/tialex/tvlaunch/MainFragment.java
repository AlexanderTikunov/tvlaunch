/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package ru.tialex.tvlaunch;

import java.util.List;

import android.app.WallpaperManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v17.leanback.app.BackgroundManager;
import android.support.v17.leanback.app.BrowseFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowPresenter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainFragment extends BrowseFragment {
    private static final String TAG = "MainFragment";

    private static final int GRID_ITEM_WIDTH = 200;
    private static final int GRID_ITEM_HEIGHT = 200;

    private final Handler mHandler = new Handler();
    private ArrayObjectAdapter mRowsAdapter;
    private Drawable mDefaultBackground;
    private DisplayMetrics mMetrics;
    private BackgroundManager mBackgroundManager;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onActivityCreated(savedInstanceState);

        prepareBackgroundManager();

        setupUIElements();

        loadRows();

        setupEventListeners();
    }

    @Override
    public void onResume() {
        super.onResume();
        setBackground();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void loadRows() {
        mRowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());
        PackageManager manager = getActivity().getPackageManager();

        // Fill prefs apps list
        Intent PrefsIntent = new Intent(Settings.ACTION_SETTINGS, null);
        List<ApplicationInfo> PrefApps = ApplicationsList.setupApps(manager, PrefsIntent);

        PrefsIntent = new Intent(Settings.ACTION_WIFI_SETTINGS, null);
        List<ApplicationInfo> NetworkPrefApps = ApplicationsList.setupApps(manager, PrefsIntent);

        // Fill TV apps list
        Intent TVintent = new Intent(Intent.ACTION_MAIN, null);
        TVintent.addCategory(Intent.CATEGORY_LEANBACK_LAUNCHER);
        List<ApplicationInfo> TVapps = ApplicationsList.setupApps(manager, TVintent);

        // Fill other apps list
        Intent OtherIntent = new Intent(Intent.ACTION_MAIN, null);
        OtherIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ApplicationInfo> OtherApps = ApplicationsList.setupApps(manager, OtherIntent);

        // Add TV apps to TVListRowAdapter
        TVAppPresenter tvAppPresenter = new TVAppPresenter();
        ArrayObjectAdapter TVListRowAdapter = new ArrayObjectAdapter(tvAppPresenter);
        for (ApplicationInfo i:TVapps)
        {
            TVListRowAdapter.add(i);
            Log.d(TAG, "Adding TV " + i.toString());
        }

        // Add TVListRowAdapter to mRowsAdapter
        if (TVListRowAdapter.size() != 0)
        {
            HeaderItem TVappsHeader = new HeaderItem(0, "TV apps");
            mRowsAdapter.add(new ListRow(TVappsHeader, TVListRowAdapter));
        }

        // Add other apps to OtherListRowAdapter
        OtherAppPresenter otherAppPresenter = new OtherAppPresenter();
        ArrayObjectAdapter OtherListRowAdapter = new ArrayObjectAdapter(otherAppPresenter);
        boolean presence_flag;
        for (ApplicationInfo i:OtherApps)
        {
            presence_flag = false;
            for (ApplicationInfo j:TVapps)
            {
                if (i.getPackageName().equals(j.getPackageName()))
                {
                    presence_flag = true;
                    break;
                }
            }
            for (ApplicationInfo j:PrefApps)
            {
                if (i.getPackageName().equals(j.getPackageName()))
                {
                    presence_flag = true;
                    break;
                }
            }
            if (!presence_flag)
            {
                OtherListRowAdapter.add(i);
                Log.d(TAG, "Adding other " + i.toString());

            }
        }

        // Add OtherListRowAdapter to mRowsAdapter
        if (OtherListRowAdapter.size() != 0)
        {
            HeaderItem OtherAppsHeader = new HeaderItem(1, "Other apps");
            mRowsAdapter.add(new ListRow(OtherAppsHeader, OtherListRowAdapter));
        }

        // Add pref apps to OtherListRowAdapter
        OtherAppPresenter prefAppPresenter = new OtherAppPresenter();
        ArrayObjectAdapter prefListRowAdapter = new ArrayObjectAdapter(prefAppPresenter);
        for (ApplicationInfo i:PrefApps)
        {
            prefListRowAdapter.add(i);
        }
        for (ApplicationInfo i:NetworkPrefApps)
        {
            prefListRowAdapter.add(i);
        }

        // Add gridRowAdapter to mRowsAdapter
        HeaderItem prefHeader = new HeaderItem(2, "Preferences");
        mRowsAdapter.add(new ListRow(prefHeader, prefListRowAdapter));

        // Setting adapter
        setAdapter(mRowsAdapter);
    }

    private void prepareBackgroundManager() {

        mBackgroundManager = BackgroundManager.getInstance(getActivity());
        mBackgroundManager.attach(getActivity().getWindow());
        mDefaultBackground = getResources().getDrawable(R.drawable.default_background);
        mMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mMetrics);
    }

    private void setupUIElements() {
        // setBadgeDrawable(getActivityClass().getResources().getDrawable(
        // R.drawable.videos_by_google_banner));
        //setTitle(getString(R.string.browse_title)); // Badge, when set, takes precedent
        // over title
        setHeadersState(HEADERS_DISABLED);
        setHeadersTransitionOnBackEnabled(true);

        // set fastLane (or headers) background color
        setBrandColor(getResources().getColor(R.color.fastlane_background));

        setBackground();
    }

    private void setupEventListeners() {
        setOnItemViewClickedListener(new ItemViewClickedListener());
    }

    protected void setBackground() {
        WallpaperManager wallpaperManager = WallpaperManager.getInstance(this.getActivity().getBaseContext());
        final Drawable wallpaper = wallpaperManager.getDrawable();
        mBackgroundManager.setDrawable(wallpaper);
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                                  RowPresenter.ViewHolder rowViewHolder, Row row) {

            if (item instanceof ApplicationInfo) {
                ApplicationInfo applicationInfo = (ApplicationInfo) item;
                Log.d(TAG, "Item: " + ((ApplicationInfo) item).getActivityClass());
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.setClassName(applicationInfo.getPackageName(), applicationInfo.getActivityClass());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                try {
                    startActivity(intent);
                }
                catch (ActivityNotFoundException e)
                {
                    e.printStackTrace();
                    Intent errorIntent = new Intent(getActivity(), BrowseErrorActivity.class);
                    startActivity(errorIntent);
                    Toast.makeText(getActivity(), applicationInfo.getActivityName() + "NOT FOUND!", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        }
    }

    private class GridItemPresenter extends Presenter {
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent) {
            TextView view = new TextView(parent.getContext());
            view.setLayoutParams(new ViewGroup.LayoutParams(GRID_ITEM_WIDTH, GRID_ITEM_HEIGHT));
            view.setFocusable(true);
            view.setFocusableInTouchMode(true);
            view.setBackgroundColor(getResources().getColor(R.color.default_background));
            view.setTextColor(Color.WHITE);
            view.setGravity(Gravity.CENTER);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, Object item) {
            ((TextView) viewHolder.view).setText((String) item);
        }

        @Override
        public void onUnbindViewHolder(ViewHolder viewHolder) {
        }
    }

}
