package ru.tialex.tvlaunch;

import android.graphics.drawable.Drawable;
import android.support.v17.leanback.widget.Presenter;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;

public class TVAppPresenter extends Presenter {
    private static final String TAG = "TVAppPresenter";

    private static final int CARD_WIDTH = 240;
    private static final int CARD_HEIGHT = 135;
    private static Drawable mDefaultImage;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        Log.d(TAG, "onCreateViewHolder");

        int sDefaultBackgroundColor = parent.getResources().getColor(R.color.default_background);
        mDefaultImage = parent.getResources().getDrawable(R.drawable.def_icon);

        ImageView imageView = new android.support.v7.widget.AppCompatImageView(parent.getContext());
        imageView.setFocusable(true);
        imageView.setFocusableInTouchMode(true);
        imageView.setBackgroundColor(sDefaultBackgroundColor);
        return new ViewHolder(imageView);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ApplicationInfo applicationInfo = (ApplicationInfo) item;
        ImageView imageView = (ImageView) viewHolder.view;

        Log.d(TAG, "onBindViewHolder");
        if (applicationInfo.getActivityClass() != null) {
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.getLayoutParams().height = CARD_HEIGHT;
            imageView.getLayoutParams().width = CARD_WIDTH;
            Drawable tempDrawable = applicationInfo.getIconDrawable();
            imageView.setImageDrawable((tempDrawable != null) ? applicationInfo.getIconDrawable() : mDefaultImage);
        }
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        Log.d(TAG, "onUnbindViewHolder");
        ImageView imageView = (ImageView) viewHolder.view;
        imageView.setImageDrawable(null);
    }
}
