package ru.tialex.tvlaunch;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class ApplicationsList {

    private static List<ApplicationInfo> list;

    public static List<ApplicationInfo> setupApps(PackageManager packageManager, Intent intent) {

        list = new ArrayList<>();

        List<ResolveInfo> availableActivities = packageManager.queryIntentActivities(intent, 0);
        for (ResolveInfo ri : availableActivities) {
            ApplicationInfo app = new ApplicationInfo();
            app.setActivityName((String)ri.loadLabel(packageManager));
            app.setPackageName(ri.activityInfo.packageName);
            app.setActivityClass(ri.activityInfo.name);
//            if (intent.hasCategory(Intent.CATEGORY_LEANBACK_LAUNCHER))
//            {app.setBannerDrawable(ri.activityInfo.loadBanner(packageManager));}
//            else {app.setIconDrawable(ri.activityInfo.loadIcon(packageManager));}
            app.setIconDrawable(ri.activityInfo.loadIcon(packageManager));
            list.add(app);
        }
        Collections.sort(list, new Comparator<ApplicationInfo>() {
            @Override
            public int compare(ApplicationInfo a, ApplicationInfo b) {
                return (a.getActivityName()).compareTo(b.getActivityName());
            }
        });
        return list;
    }
}